package br.com.avenue.service;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import br.com.avenue.model.entity.Product;

public class ProductServiceTest {
	
	private ProductService service;
	
	@Before
	public void init() {
		this.service = new ProductService();
	}
	
	@Test
	public void UpdateProductSuccess() {
		Product product = new Product();
		product.setName("Product test");
		product.setId(1);
		
		service.persistProduct(product);
		
		product.setName("Product test 2");
		
		service.updateProduct(product);
		
		Product newProduct = service.getProductById(1);
		assertEquals(newProduct.getName(), "Product test 2");
	}

}
