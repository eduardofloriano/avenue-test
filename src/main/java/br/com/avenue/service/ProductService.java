package br.com.avenue.service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.avenue.model.entity.Product;
import br.com.avenue.test.model.dao.ProductDAO;

public class ProductService {

	private ProductDAO dao;
	
	public ProductService() {
		EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("avenue-postgresql");
		dao = new ProductDAO(emFactory.createEntityManager());
	}
	
	public Product getProductById(Integer id) {
		return dao.findById(id);
	}
	
	public Product updateProduct(Product product) {
		dao.merge(product);
		return product;
	}

	public void deleteProduct(Integer id) {
		dao.remove(id);
	}
	
	public void persistProduct(Product product) {
		dao.persist(product);
	}
	
}
