package br.com.avenue.service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.avenue.model.entity.Image;
import br.com.avenue.test.model.dao.ImageDAO;

public class ImageService {

	private ImageDAO dao;
	
	public ImageService() {
		EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("tarefas");
		dao = new ImageDAO(emFactory.createEntityManager());
	}
	
	public Image getImageById(Integer id) {
		return dao.findById(id);
	}
	
	public Image updateImage(Image image) {
		dao.merge(image);
		return image;
	}

	public void deleteImage(Integer id) {
		dao.remove(id);
	}
	
}
