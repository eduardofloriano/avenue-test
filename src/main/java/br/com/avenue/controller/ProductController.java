package br.com.avenue.controller;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.avenue.model.entity.Product;
import br.com.avenue.service.ProductService;

@Path("/products")
public class ProductController {

	ProductService productService = new ProductService();

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductById(@PathParam("id") int id) {
		productService.getProductById(id);
		return null;
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Product updateProduct(Product country) {
		return productService.updateProduct(country);
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteCountry(@PathParam("id") int id) {
		productService.deleteProduct(id);
	}
	
	
}
