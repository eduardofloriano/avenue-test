package br.com.avenue.controller;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.avenue.model.entity.Image;
import br.com.avenue.model.entity.Product;
import br.com.avenue.service.ImageService;

@Path("/images")
public class ImageController {

	ImageService imageService = new ImageService();

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getImageById(@PathParam("id") int id) {
		imageService.getImageById(id);
		return null;
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Image updateImage(Image image) {
		return imageService.updateImage(image);
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteImage(@PathParam("id") int id) {
		imageService.deleteImage(id);
	}
}
