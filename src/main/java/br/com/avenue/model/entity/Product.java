package br.com.avenue.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


@NamedQueries({
	@NamedQuery(name = "GET_ALL_PRODUCTS_WITHOUT_RELATIONS", query = "select p from Product p")
})
@Entity
public class Product {

	public static final String GET_ALL_PRODUCTS_WITHOUT_RELATIONS = "getAllProductsWithoutRelations";
	
	@Id
	private long id;
	private String name;
	
	@OneToMany(mappedBy="product")
	private List<Image> images;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}
	
	
	
}
