package br.com.avenue.test.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.avenue.model.entity.Product;

public class ProductDAO {

	private EntityManager em;
	private DAO<Product> dao;
	
	public ProductDAO (EntityManager em) {
		this.em = em;
		this.dao = new DAO<Product>(Product.class, em);
	}
	
	public Product findById(Integer id) {
		return dao.findById(id);
	}
	
	public void merge(Product product) {
		dao.merge(product);
	}
	
	public void remove(int id) {
		Product product = dao.findById(id);
		dao.remove(product);
	}
	
	public void persist(Product product) {
		dao.persist(product);
	}
	
	public List<Product> getAllProductsWithoutRelations(){
		TypedQuery<Product> query = em.createNamedQuery(Product.GET_ALL_PRODUCTS_WITHOUT_RELATIONS, Product.class);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
	
}
