package br.com.avenue.test.model.dao;

import javax.persistence.EntityManager;

import br.com.avenue.model.entity.Image;

public class ImageDAO {

	private DAO<Image> dao;
	
	public ImageDAO (EntityManager em) {
		this.dao = new DAO<Image>(Image.class, em);
	}
	
	public Image findById(Integer id) {
		return dao.findById(id);
	}
	
	public void merge(Image image) {
		dao.merge(image);
	}
	
	public void remove(int id) {
		Image image = dao.findById(id);
		dao.remove(image);
	}
	
}
